// base config from jest-present-angular
const jestPreset = require('jest-preset-angular/jest-preset');

const { globals } = jestPreset;
const tsjest = globals['ts-jest'];

// set the correct path to the spect ts-config file
// the default for the jest-preset-angular package
// points to an incorrect path:
// <rootDir/src/tsconfig.spec.js
const tsjestOverrides = {
    ...tsjest,
    tsConfig: '<rootDir>/src/tsconfig.spec.json',
};

const globalOverrides = {
    ...globals,
    'ts-jest': { ...tsjestOverrides },
};

// make sure to add in the required preset and
// and setup file entries
module.exports = {
    ...jestPreset,
    globals: { ...globalOverrides },
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/src/setupJest.ts'],
    transform: {
        '^.+\\.(ts|js|html)$': 'ts-jest',
    },
    moduleNameMapper: {
        '@actions/(.*)': '<rootDir>/src/app/application/actions/$1',
        '@selectors/(.*)': '<rootDir>/src/app/application/selectors/$1',
        '@effects/(.*)': '<rootDir>/src/app/application/effects/$1',
    },
    // Ionic fix https://github.com/thymikee/jest-preset-angular/issues/277
    transformIgnorePatterns: ['/node_modules/(?!@ionic|ngx-socket-io/).+\\.js$'],
};
