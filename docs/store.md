# Store

Store, von der RxJs Programmbibliothek, ermöglicht einen globalen State für die App zu definieren und zu verwalten. Anhand des States können in bestimmten Bereichen der Anwendung Inhalte angezeigt, versteckt oder verändert werden.
Hier ist die offiziele [Dokumentation](https://ngrx.io/guide/store)

## Schlüsselkonzepte

-   **Store** ist ein Observable, der den Zugriff auf den State ermöglicht

-   **Selectors** sind "pure functions" mit denen man eine bestimmte Information, aus dem State, herausnehmen kann.

-   **Actions** sind einzigartige Events die in Components oder Services geworfen werden.

-   **Reducers** sind "pure functions" die, wenn eine _Action_ geworfen wrid, den globalen State ändern

-   **Effects** sind Services die auf ein bestimmtes _Action_ reagieren und ein neues zurückgeben.
    Die werden benutzt, um komplexe Logik in den Components auszulagern.

## Zustandsdiagramme

[Daily-Challenge-Statechart](./store-statechart/Daily-Challenge-State.jpg)
