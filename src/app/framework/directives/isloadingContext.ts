/* eslint-disable @typescript-eslint/no-non-null-assertion */
export class IsLoadingContext<T = unknown> {
    // tslint:disable-next-line: no-non-null-assertion
    public appIsloading: T = null!;
}
