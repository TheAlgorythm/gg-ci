import { IonSpinner } from '@ionic/angular';
import { IsloadingDirective } from './IsloadingDirective';
import { createDirectiveFactory, SpectatorDirective, HostComponent } from '@ngneat/spectator';

describe('IsloadingDirective', () => {
    const createDirective = createDirectiveFactory({
        directive: IsloadingDirective,
        entryComponents: [IonSpinner],
    });

    let spectator: SpectatorDirective<IsloadingDirective>;

    it('should create the app', () => {
        spectator = createDirective('<div *appIsloading="true"><span>test</span></div>');
        const directive = spectator.directive;
        expect(directive).toBeTruthy();
    });

    it('should create child view if context exist', () => {
        spectator = createDirective('<div *appIsloading="true"><span>test</span></div>');
        const children = spectator.debugElement.children[0];
        expect(children.componentInstance).toBeInstanceOf(HostComponent);
    });

    it('should create ionspinner if context not exist', () => {
        spectator = createDirective('<div *appIsloading=""><span>test</span></div>');
        const children = spectator.debugElement.children[0];
        expect(children.componentInstance).toBeInstanceOf(IonSpinner);
    });
});
