import { DetailChallengeComponent } from './detail-challenge.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dispatcher } from 'app/framework/dispatcher';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';
import { AlertController } from '@ionic/angular';
import { Challenge } from 'app/domain/entities/challenge';

describe('DetailChallengeComponent', () => {
    const htmlAlertElementMock = {
        present: jest.fn(),
    };
    const alertControllerMock = {
        create: jest.fn().mockResolvedValue(htmlAlertElementMock),
    };
    const createComponent = createComponentFactory({
        component: DetailChallengeComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Dispatcher],
        declarations: [TransformToCategoryPipe],
        providers: [{ provide: AlertController, useValue: alertControllerMock }],
        detectChanges: false,
    });
    let spectator: Spectator<DetailChallengeComponent>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should open a modal at click on a challenge', async () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        spectator.setInput({ challenge });
        spectator.click('#done_button');

        expect(alertControllerMock.create).toHaveBeenCalledTimes(1);

        const alertElement = await alertControllerMock.create();
        expect(alertElement.present).toHaveBeenCalledTimes(1);
    });
});
