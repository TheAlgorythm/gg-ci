import { AlertElement } from './alert-element-mock';

export class AlertControllerMock {
    public target = '';
    public callback: any = null;

    constructor() {}

    async create(opts: any): Promise<AlertElement> {
        return new Promise<AlertElement>(() => {
            for (const button of opts.buttons) {
                if (button.role === this.target) {
                    console.log('Chose Button');
                    return new AlertElement(button.handler, this.callback);
                }
            }
        });
    }
}
