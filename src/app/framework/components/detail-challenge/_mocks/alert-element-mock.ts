export class AlertElement {
    constructor(private handler: () => void, private callback: any) {}

    async present(): Promise<void> {
        return new Promise<void>(() => {
            this.handler();
            if (this.callback != null) {
                this.callback();
            }
        });
    }
}
