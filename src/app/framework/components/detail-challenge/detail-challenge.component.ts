import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { openCloseAnimation } from 'app/animations/selected-open-close';
import { Challenge } from 'app/domain/entities/challenge';
import { IonSlides, AlertController } from '@ionic/angular';
import { Dispatcher } from 'app/framework/dispatcher';
import { Action } from '@ngrx/store';

@Component({
    selector: 'app-detail-challenge',
    templateUrl: './detail-challenge.component.html',
    styleUrls: ['./detail-challenge.component.scss'],
    animations: [openCloseAnimation],
})
export class DetailChallengeComponent implements OnInit {
    @Input() challenge!: Challenge;
    @Input() completeAction!: Action;
    @ViewChild('hintSlides') hintSlides!: IonSlides;

    selectedHintTab = 0;
    slideHintFieldsOpts = {
        initialSlide: this.selectHintTab,
        speed: 400,
    };

    constructor(private dispatcher: Dispatcher, private alertController: AlertController) {}

    ngOnInit(): void {}

    async finish(): Promise<void> {
        const alert = await this.alertController.create({
            header: 'Bestätigen',
            message: 'Haben Sie die Challenge erledigt?',
            buttons: [
                {
                    text: 'Abbrechen',
                    role: 'Cancel',
                    cssClass: 'button',
                },
                {
                    text: 'Bestätigen',
                    role: 'Ok',
                    handler: (): void => {
                        this.dispatcher.dispatch(this.completeAction);
                    },
                },
            ],
        });

        await alert.present();
    }

    async selectHintTab(id: number): Promise<void> {
        this.selectedHintTab = id;
        await this.hintSlides.slideTo(this.selectedHintTab);
    }

    async swipeHint(): Promise<void> {
        this.selectedHintTab = await this.hintSlides.getActiveIndex();
    }
}
