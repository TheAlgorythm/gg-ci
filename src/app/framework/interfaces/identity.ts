import { Stringifyable } from './stringifyable';

export interface Identity<T, TType extends Stringifyable> {
    id: TType;
}

export type SequentialIdentity<T> = Identity<T, number>;
export type RandomIdentity<T> = Identity<T, string>;
