import { Observable } from 'rxjs';

export interface Source<TOut> {
    out$: Observable<TOut>;
}
