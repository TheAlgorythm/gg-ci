import { Pipe, PipeTransform } from '@angular/core';
import { Category } from 'app/domain/enums/category';

@Pipe({
    name: 'transformToCategory',
})
export class TransformToCategoryPipe implements PipeTransform {
    transform(categoryId: number): Category | null {
        if (categoryId in Category.categories) {
            return Category.categories[categoryId];
        }
        return null;
    }
}
