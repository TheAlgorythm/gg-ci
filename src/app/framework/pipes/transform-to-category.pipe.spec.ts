import { Category } from './../../domain/enums/category';
import { TransformToCategoryPipe } from './transform-to-category.pipe';

describe('TransformToCategoryPipe', () => {
    it('create an instance', () => {
        const pipe = new TransformToCategoryPipe();
        expect(pipe).toBeTruthy();
    });

    it('should return category if category id exists', () => {
        const pipe = new TransformToCategoryPipe();
        expect(pipe.transform(1)).toBe(Category.categories[1]);
    });

    it('should return null if category id not exist', () => {
        const pipe = new TransformToCategoryPipe();
        expect(pipe.transform(Category.categories.length)).toBe(null);
    });
});
