import { TransformToCategoryPipe } from './../pipes/transform-to-category.pipe';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IsloadingDirective } from '../directives/IsloadingDirective';
import { DetailChallengeComponent } from '../components/detail-challenge/detail-challenge.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [IsloadingDirective, TransformToCategoryPipe, DetailChallengeComponent],
    exports: [IsloadingDirective, TransformToCategoryPipe, DetailChallengeComponent],
    imports: [CommonModule, IonicModule],
})
export class SharedModule {}
