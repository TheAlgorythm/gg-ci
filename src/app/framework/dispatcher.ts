import { Store, Action } from '@ngrx/store';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class Dispatcher {
    constructor(private store: Store<any>) {}

    public dispatch(action: Action): void {
        this.store.dispatch(action);
    }
}
