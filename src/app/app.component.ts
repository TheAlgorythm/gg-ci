import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Dispatcher } from './framework/dispatcher';
import { initialize } from '@actions/global/initialize';
import { GlobalModelState } from './application/reducers/global/global.state';
import { Observable } from 'rxjs';
import { GlobalStateSource } from './framework/sources/globalStateSource';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
})
export class AppComponent implements OnInit {
    public viewModel$!: Observable<GlobalModelState>;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private dispatcher: Dispatcher,
        private source: GlobalStateSource,
    ) {}

    ngOnInit(): void {
        this.initializeApp();
        this.dispatcher.dispatch(initialize());
        this.viewModel$ = this.source.out$;
    }

    initializeApp(): void {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
}
