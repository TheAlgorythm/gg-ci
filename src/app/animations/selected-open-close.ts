import { trigger, transition, state, animate, style, query, animateChild, group } from '@angular/animations';

export const openCloseAnimation = [
    trigger('cardContentTrigger', [
        state(
            'open',
            style({
                height: 'auto',
                opacity: 1,
            }),
        ),
        state(
            'close',
            style({
                height: 0,
                opacity: 0,
                padding: 0,
            }),
        ),
        transition('* => *', [animate('1s ease')]),
    ]),
    // Parent triggers
    trigger('imageWrapperTrigger', [
        state(
            'image-wrapper-small',
            style({
                marginTop: '15%',
                height: '15%',
            }),
        ),
        state(
            'void',
            style({
                marginTop: '30%',
                height: '35%',
            }),
        ),
        transition('* => *', [
            group([query('@imageTrigger', animateChild()), query('@titleTrigger', animateChild()), animate('1s ease')]),
        ]),
    ]),
    trigger('openCloseTrigger', [
        state(
            'open',
            style({
                backgroundColor: '#b9cd95',
                height: '100%',
            }),
        ),
        state(
            'close',
            style({
                backgroundColor: '#CEDDB6',
                height: '0',
            }),
        ),
        transition('* => *', [
            group([
                query('@textTrigger', animateChild()),
                query('@triangleTrigger', animateChild()),
                query('@lineTrigger', animateChild()),
                animate('1s ease'),
            ]),
        ]),
    ]),
    // imageWrapperTrigger children
    trigger('imageTrigger', [
        state(
            'image-small',
            style({
                transform: 'translateX(-35vw)',
                width: '20%',
            }),
        ),
        state(
            'void',
            style({
                transform: 'translateX(0)',
                width: '40%',
            }),
        ),
        transition('* => *', [animate('1s ease')]),
    ]),
    trigger('titleTrigger', [
        state(
            'title-small',
            style({
                transform: 'translate(10vw, -10vh)',
            }),
        ),
        state(
            'void',
            style({
                transform: 'translate(0)',
            }),
        ),
        transition('* => *', [animate('1s ease')]),
    ]),
    // openCloseTrigger children
    trigger('lineTrigger', [
        state(
            'dark',
            style({
                backgroundColor: '#CEDDB6',
            }),
        ),
        state(
            'bright',
            style({
                backgroundColor: '#b9cd95',
            }),
        ),
    ]),
    trigger('triangleTrigger', [
        state(
            'dark',
            style({
                borderTop: '20px solid #CEDDB6',
            }),
        ),
        state(
            'bright',
            style({
                borderTop: '20px solid #b9cd95',
            }),
        ),
    ]),
    trigger('textTrigger', [
        state(
            'openText',
            style({
                opacity: 1,
            }),
        ),
        state(
            'closeText',
            style({
                opacity: 0,
            }),
        ),
    ]),
];
