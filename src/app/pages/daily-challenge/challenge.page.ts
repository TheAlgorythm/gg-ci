import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DailyChallengeState } from 'app/application/reducers/daily-challenges/daily-challenge.state';
import { Dispatcher } from 'app/framework/dispatcher';
import { DailyChallengeViewModel } from './viewModel/daily-challenge.viewmodel';
import { DailyChallengeSource } from './sources/daily-challenge.source';
import { loadChallengeState } from '@actions/daily-challenges';

@Component({
    selector: 'app-challenge',
    templateUrl: 'challenge.page.html',
    styleUrls: ['challenge.page.scss'],
})
export class ChallengePage implements OnInit {
    public viewModel$: Observable<DailyChallengeViewModel>;

    public enumValues = DailyChallengeState;

    constructor(private source: DailyChallengeSource, private dispatcher: Dispatcher) {
        this.viewModel$ = this.source.out$;
    }

    ngOnInit(): void {}

    ionViewWillEnter(): void {
        this.dispatcher.dispatch(loadChallengeState());
    }
}
