import { ChallengePage } from './challenge.page';

import { Dispatcher } from 'app/framework/dispatcher';
import { DailyChallengeSource } from './sources/daily-challenge.source';
import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { MockComponent, MockDirective } from 'ng-mocks';
import { IsloadingDirective } from '../../framework/directives/IsloadingDirective';
import { SelectedComponent } from './components/selected/selected.component';
import { ChoiceComponent } from './components/choice/choice.component';
import { CompleteComponent } from './components/complete/complete.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { loadChallengeState } from '@actions/daily-challenges';

describe('ChallengeComponent', () => {
    const createComponent = createComponentFactory({
        component: ChallengePage,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [DailyChallengeSource, Dispatcher],
        declarations: [
            MockDirective(IsloadingDirective),
            MockComponent(SelectedComponent),
            MockComponent(ChoiceComponent),
            MockComponent(CompleteComponent),
        ],
        detectChanges: false,
    });
    let spectator: Spectator<ChallengePage>;
    let dispatcherSpy: SpyObject<Dispatcher>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should initialize the app', () => {
        const app = spectator.component;
        dispatcherSpy = spectator.inject(Dispatcher);

        app.ionViewWillEnter();

        expect(dispatcherSpy.dispatch).toHaveBeenCalledTimes(1);
        expect(dispatcherSpy.dispatch).toHaveBeenCalledWith(loadChallengeState());
        expect(app).toBeTruthy();
    });
});
