import {
    DailyChallengeState,
    DailyChallengeModelState,
} from 'app/application/reducers/daily-challenges/daily-challenge.state';
import { Challenge } from 'app/domain/entities/challenge';

export class DailyChallengeViewModel {
    state?: DailyChallengeState;
    current?: Challenge | [Challenge, Challenge] | Date;
    lastAction: Date;
    isLoading: boolean;
    startDate?: Date | null;

    constructor(data: DailyChallengeModelState = new DailyChallengeModelState()) {
        if (data.state) {
            this.current = data.current;
            this.isLoading = false;
            this.lastAction = data.lastChange;
            this.state = data.state;
            this.startDate = data.startDate;
        } else {
            this.lastAction = data.lastChange;
            this.isLoading = true;
        }
    }
}
