import { Challenge } from './../../../../domain/entities/challenge';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SelectedComponent } from './selected.component';
import { DetailChallengeComponent } from 'app/framework/components/detail-challenge/detail-challenge.component';
import { complete } from '@actions/daily-challenges';
import { MockComponent } from 'ng-mocks';

describe('SelectedComponent', () => {
    const createComponent = createComponentFactory({
        component: SelectedComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        declarations: [MockComponent(DetailChallengeComponent)],
        detectChanges: false,
    });
    let spectator: Spectator<SelectedComponent>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should return complete action', () => {
        const startDate = new Date();
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const completeAction = complete({ challenge, startDate });
        spectator.setInput({ challenge, startDate });

        expect(spectator.component.getCompleteAction()).toEqual(completeAction);
    });
});
