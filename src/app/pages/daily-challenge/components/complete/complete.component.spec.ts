import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CompleteComponent } from './complete.component';

describe('CompleteComponent', () => {
    const createComponent = createComponentFactory({
        component: CompleteComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        detectChanges: false,
    });
    let spectator: Spectator<CompleteComponent>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });
});
