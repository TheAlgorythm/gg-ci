import { Category } from 'app/domain/enums/category';
import { Component, OnInit, Input } from '@angular/core';
import { Challenge } from 'app/domain/entities/challenge';
import { Dispatcher } from 'app/framework/dispatcher';
import { selectChoice } from '@actions/daily-challenges';

@Component({
    selector: 'app-choice',
    templateUrl: './choice.component.html',
    styleUrls: ['./choice.component.scss'],
})
export class ChoiceComponent implements OnInit {
    @Input() choices: [Challenge, Challenge] | undefined;
    @Input() startDate!: Date;

    selectedChallenge: Challenge = new Challenge({});

    categories: Category[] = Category.categories;

    constructor(private dispatcher: Dispatcher) {}

    ngOnInit(): void {}

    select(challenge: Challenge): void {
        if (challenge === this.selectedChallenge) {
            const selectChoiceAction = selectChoice({ challenge, startDate: this.startDate });
            this.dispatcher.dispatch(selectChoiceAction);
        } else {
            this.selectedChallenge = challenge;
        }
    }
}
