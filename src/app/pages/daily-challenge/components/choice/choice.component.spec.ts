import { createComponentFactory, Spectator, SpyObject } from '@ngneat/spectator/jest';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ChoiceComponent } from './choice.component';
import { Challenge } from 'app/domain/entities/challenge';
import { Dispatcher } from 'app/framework/dispatcher';
import { selectChoice } from '@actions/daily-challenges';
import { TransformToCategoryPipe } from 'app/framework/pipes/transform-to-category.pipe';

describe('ChoiceComponent', () => {
    const createComponent = createComponentFactory({
        component: ChoiceComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        mocks: [Dispatcher],
        declarations: [TransformToCategoryPipe],
        detectChanges: false,
    });
    let spectator: Spectator<ChoiceComponent>;
    let dispatcherSpy: SpyObject<Dispatcher>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should show 2 challenges', () => {
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const challenge2 = new Challenge({
            id: 2,
            title: 'i am awesome too',
            shortDescription: 'a am batman too?',
            categoryId: 42,
        });

        dispatcherSpy = spectator.inject(Dispatcher);
        spectator.setInput({
            choices: [challenge, challenge2],
        });

        const choiceElements = spectator.queryAll('ion-card');
        expect(choiceElements.length).toBe(2);
    });

    it('should dispatch a select choice action if clicked twice on a challenge', () => {
        const startDate = new Date();
        const challenge = new Challenge({
            id: 1,
            title: 'i am awesome',
            shortDescription: 'i am batman',
            categoryId: 42,
        });
        const challenge2 = new Challenge({
            id: 2,
            title: 'i am awesome too',
            shortDescription: 'a am batman too?',
            categoryId: 42,
        });

        dispatcherSpy = spectator.inject(Dispatcher);
        spectator.setInput({
            choices: [challenge, challenge2],
            startDate,
        });

        const choiceElements = spectator.queryAll('ion-card');
        spectator.click(choiceElements[0]);
        spectator.click(choiceElements[0]);

        expect(dispatcherSpy.dispatch).toHaveBeenCalledTimes(1);
        expect(dispatcherSpy.dispatch).toHaveBeenCalledWith(selectChoice({ challenge, startDate }));
    });
});
