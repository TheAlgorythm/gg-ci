import { WeeklyChallengeModelState } from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { WeeklyChallengeViewModel } from '../viewModel/weekly-challenge.viewmodel';
import { OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { FeatureSource } from 'app/framework/sources/featureSource';
import { Store } from '@ngrx/store';
import { GlobalModelState } from 'app/application/reducers/global/global.state';
import { getWeeklyChallengeState } from '@selectors/weekly-challenges/selector';

const mapViewModel: OperatorFunction<WeeklyChallengeModelState, WeeklyChallengeViewModel> = source =>
    source.pipe(map(state => new WeeklyChallengeViewModel(state)));

@Injectable()
export class WeeklyChallengeSource extends FeatureSource<WeeklyChallengeModelState, WeeklyChallengeViewModel> {
    constructor(store: Store<GlobalModelState>) {
        super(store, mapViewModel, getWeeklyChallengeState);
    }
}
