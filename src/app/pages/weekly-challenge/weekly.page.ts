import { Component, OnInit } from '@angular/core';
import { Dispatcher } from 'app/framework/dispatcher';
import { loadWeeklyChallengeState } from '@actions/weekly-challenges';
import { WeeklyChallengeState } from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { WeeklyChallengeSource } from './sources/weekly.source';
import { WeeklyChallengeViewModel } from './viewModel/weekly-challenge.viewmodel';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-weekly-challenge',
    templateUrl: './weekly.page.html',
    styleUrls: ['./weekly.page.scss'],
})
export class WeeklyPage implements OnInit {
    public viewModel$: Observable<WeeklyChallengeViewModel>;

    public weeklyState = WeeklyChallengeState;

    constructor(private source: WeeklyChallengeSource, private dispatcher: Dispatcher) {
        this.viewModel$ = source.out$;
    }

    ngOnInit(): void {}

    ionViewWillEnter(): void {
        this.dispatcher.dispatch(loadWeeklyChallengeState());
    }
}
