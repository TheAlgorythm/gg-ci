import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WeeklyPage } from './weekly.page';
import { TeaserComponent } from './components/teaser/teaser.component';
import { ActiveComponent } from './components/active/active.component';
import { WeeklyRoutingModule } from './weekly-routing.module';
import { StoreModule } from '@ngrx/store';
import { weeklyChallengeFeature } from '@selectors/weekly-challenges/selector';
import { reducer } from 'app/application/reducers/weekly-challenges';
import { WeeklyChallengeSource } from './sources/weekly.source';
import { EffectsModule } from '@ngrx/effects';
import { LoadStateEffect } from '@effects/weekly-challenges/load-state.effect';
import { SharedModule } from 'app/framework/shared/shared.module';
import { LoadTeaserEffect } from '@effects/weekly-challenges/load-teaser.effect';
import { CompleteComponent } from './components/complete/complete.component';
import { TimeEffect } from '@effects/weekly-challenges/time.effect';
import { CompleteEffect } from '@effects/weekly-challenges/completeEffect';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        WeeklyRoutingModule,
        EffectsModule.forFeature([LoadStateEffect, LoadTeaserEffect, CompleteEffect, TimeEffect]),
        StoreModule.forFeature(weeklyChallengeFeature, reducer),
    ],
    providers: [WeeklyChallengeSource],
    declarations: [WeeklyPage, TeaserComponent, ActiveComponent, CompleteComponent],
})
export class WeeklyPageModule {}
