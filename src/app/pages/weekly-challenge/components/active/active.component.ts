import { Component, OnInit, Input } from '@angular/core';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { completeChallenge } from '@actions/weekly-challenges';
import { Action } from '@ngrx/store';

@Component({
    selector: 'app-active',
    templateUrl: './active.component.html',
    styleUrls: ['./active.component.css'],
})
export class ActiveComponent implements OnInit {
    @Input() challenge!: WeeklyChallenge;
    @Input() startDate!: Date;

    constructor() {}

    ngOnInit(): void {
        if (!this.challenge) {
            throw new TypeError('The input "challenge" is required');
        }
        if (!this.startDate) {
            throw new TypeError('The input "startDate" is required');
        }
    }

    getCompleteAction(): Action {
        return completeChallenge({
            weeklyChallenge: this.challenge,
            startDate: this.startDate,
        });
    }
}
