import { createComponentFactory, Spectator } from '@ngneat/spectator';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { ActiveComponent } from './active.component';
import { MockComponent } from 'ng-mocks';
import { DetailChallengeComponent } from 'app/framework/components/detail-challenge/detail-challenge.component';
import { completeChallenge } from '@actions/weekly-challenges';

describe('ActiveComponent', () => {
    const createComponent = createComponentFactory({
        component: ActiveComponent,
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
        declarations: [MockComponent(DetailChallengeComponent)],
        detectChanges: false,
    });
    let spectator: Spectator<ActiveComponent>;

    beforeEach(() => (spectator = createComponent()));

    it('should create', () => {
        const app = spectator.component;
        expect(app).toBeTruthy();
    });

    it('should throw an error if input value challenge is not defined', () => {
        spectator.setInput({ startDate: new Date() });
        expect(() => spectator.detectChanges()).toThrowError('The input "challenge" is required');
    });

    it('should throw an error if input value date is not defined', () => {
        const challenge = new WeeklyChallenge({});
        spectator.setInput({ challenge });
        expect(() => spectator.detectChanges()).toThrowError('The input "startDate" is required');
    });

    it('should return complete action', () => {
        const weeklyChallenge = new WeeklyChallenge({});
        const startDate = new Date();
        const completeAction = completeChallenge({ weeklyChallenge, startDate });
        spectator.setInput({ challenge: weeklyChallenge, startDate });

        expect(spectator.component.getCompleteAction()).toEqual(completeAction);
    });
});
