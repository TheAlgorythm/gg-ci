import { Component, OnInit, Input } from '@angular/core';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { Dispatcher } from 'app/framework/dispatcher';
import { loadActiveChallenge } from '@actions/weekly-challenges';

@Component({
    selector: 'app-teaser',
    templateUrl: './teaser.component.html',
    styleUrls: ['./teaser.component.scss'],
})
export class TeaserComponent implements OnInit {
    @Input() challenge!: WeeklyChallenge;
    @Input() startDate!: Date;

    selected = false;

    constructor(private dispatcher: Dispatcher) {}

    ngOnInit(): void {
        if (!this.challenge) {
            throw new TypeError('The input "challenge" is required');
        }
        if (!this.startDate) {
            throw new TypeError('The input "startDate" is required');
        }
    }

    select(): void {
        if (false === this.selected) {
            this.selected = true;
            return;
        }
        const loadActiveChallengeAction = loadActiveChallenge({
            weeklyChallenge: this.challenge,
            startDate: this.startDate,
        });
        this.dispatcher.dispatch(loadActiveChallengeAction);
    }
}
