import { OnInit, Component } from '@angular/core';
import { Category } from 'app/domain/enums/category';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { switchMap } from 'rxjs/operators';
import { ChallengeCounter } from './models/challenge-counter.model';
import { Observable, of } from 'rxjs';
import { Challenge } from 'app/domain/entities/challenge';

@Component({
    selector: 'app-account',
    templateUrl: './account.page.html',
    styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
    public categories = Category.categories;
    public challengeCounter$!: Observable<ChallengeCounter>;

    constructor(private challengeHistoryRepository: ChallengeHistoryRepository) {}

    ngOnInit(): void {}

    ionViewWillEnter(): void {
        this.countChallenges();
    }

    private countChallenges(): void {
        this.challengeCounter$ = this.challengeHistoryRepository.getCompletedChallenges().pipe(
            switchMap(challenges => {
                const challengeCounter = new ChallengeCounter();

                if (null === challenges) {
                    return of(challengeCounter);
                }
                const dailyChallenges = challenges[0];
                const weeklyChallenges = challenges[1];

                challengeCounter.completedWeeklyChallenge =
                    challengeCounter.completedWeeklyChallenge + weeklyChallenges.length;
                if (Array.isArray(challenges)) {
                    for (const challenge of dailyChallenges) {
                        this.count(challenge, challengeCounter);
                    }
                }
                return of(challengeCounter);
            }),
        );
    }

    private count(challenge: Challenge, challengeCounter: ChallengeCounter): void {
        switch (challenge.categoryId) {
            case Category.CONSUME.id: {
                challengeCounter.completedConsumeChallengeCount++;
                break;
            }
            case Category.DIET.id: {
                challengeCounter.completedDietChallengeCount++;
                break;
            }
            case Category.MOBILITY.id: {
                challengeCounter.completedMobilityChallengeCount++;
                break;
            }
            case Category.HOUSHOLD.id: {
                challengeCounter.completedHouseholdChallengeCount++;
                break;
            }
            case Category.KNOWLEDGE.id: {
                challengeCounter.completedKnowledgeChallengeCount++;
                break;
            }
        }
    }
}
