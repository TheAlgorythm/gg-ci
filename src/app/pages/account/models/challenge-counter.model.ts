export class ChallengeCounter {
    completedWeeklyChallenge = 1;

    completedConsumeChallengeCount = 0;
    completedDietChallengeCount = 0;
    completedMobilityChallengeCount = 0;
    completedHouseholdChallengeCount = 0;
    completedKnowledgeChallengeCount = 0;

    calculateScore(): number {
        return (
            this.completedWeeklyChallenge *
            (this.completedConsumeChallengeCount +
                this.completedDietChallengeCount +
                this.completedMobilityChallengeCount +
                this.completedHouseholdChallengeCount +
                this.completedKnowledgeChallengeCount)
        );
    }
}
