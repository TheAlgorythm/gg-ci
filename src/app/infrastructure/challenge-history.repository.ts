import { PouchRepository } from './pouch.repository';
import { ChallengeHistory } from 'app/domain/entities/challenge-history';
import { Challenge, Choice } from 'app/domain/entities/challenge';
import { HistoryState, ChallengeType } from 'app/domain/enums/history.enum';
import { Injectable, Inject } from '@angular/core';
import { SequentialIdentity } from 'app/framework/interfaces/identity';
import { switchMap, defaultIfEmpty } from 'rxjs/operators';
import { CHALLENGECRUD } from './challenge-crud';
import { Repository } from 'app/framework/interfaces/repository';
import { Observable, forkJoin, of } from 'rxjs';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { WEEKLYCHALLENGECRUD } from './weekly-challenge.repository';

@Injectable({
    providedIn: 'root',
})
export class ChallengeHistoryRepository extends PouchRepository<ChallengeHistory, string> {
    constructor(
        @Inject(CHALLENGECRUD) private dailyRepository: Repository<Challenge, number>,
        @Inject(WEEKLYCHALLENGECRUD) private weeklyRepository: Repository<WeeklyChallenge, number>,
    ) {
        super('challengeHistory');
    }

    complete(challenge: SequentialIdentity<Challenge>, type: ChallengeType, state: HistoryState): void {
        const historyEntry = new ChallengeHistory(challenge.id, type, state, new Date());

        this.add(historyEntry);
    }

    completeChoice(choice: Choice, type: ChallengeType, state: HistoryState): void {
        choice.forEach(challenge => {
            const historyEntry = new ChallengeHistory(challenge.id, type, state, new Date());
            this.add(historyEntry);
        });
    }

    getCompletedChallenges(): Observable<[Challenge[], WeeklyChallenge[]] | null> {
        return this.all().pipe(
            switchMap(challengeHistory => {
                const dailyChallenges$: Observable<Challenge>[] = [];
                const weeklyChallenges$: Observable<WeeklyChallenge>[] = [];

                challengeHistory.forEach((challengeRow: ChallengeHistory) => {
                    if (challengeRow.state !== HistoryState.Success) {
                        return;
                    }
                    if (challengeRow.type === ChallengeType.Daily) {
                        const dailyChallenge = this.dailyRepository.get({ id: challengeRow.challengeId });
                        dailyChallenges$.push(dailyChallenge);
                    }
                    if (challengeRow.type === ChallengeType.Weekly) {
                        weeklyChallenges$.push(this.weeklyRepository.get({ id: challengeRow.challengeId }));
                    }
                });
                const dailyChallenges = dailyChallenges$.length > 0 ? forkJoin(dailyChallenges$) : of([]);
                const weeklyChallenges = weeklyChallenges$.length > 0 ? forkJoin(weeklyChallenges$) : of([]);

                return forkJoin([dailyChallenges, weeklyChallenges]);
            }),
        );
    }
}
