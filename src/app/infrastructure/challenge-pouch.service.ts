import { ChallengeService } from 'app/application/services/challenge.service';
import { Observable } from 'rxjs';
import { Challenge, Choice } from 'app/domain/entities/challenge';
import { map } from 'rxjs/operators';
import { Inject } from '@angular/core';
import { Repository } from 'app/framework/interfaces/repository';
import { CHALLENGECRUD } from './challenge-crud';

export class ChallengePouchService implements ChallengeService {
    constructor(@Inject(CHALLENGECRUD) private repo: Repository<Challenge, number>) {}

    getChoice(): Observable<Choice> {
        return this.repo
            .all()
            .pipe(map(cs => [cs[this.getRandomIndex(cs.length)], cs[this.getRandomIndex(cs.length)]]));
    }

    private getRandomIndex(length: number): number {
        return Math.floor(Math.random() * length);
    }
}
