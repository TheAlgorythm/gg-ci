import { createRepositoryToken } from 'app/framework/interfaces/repository';
import { PouchRepository } from './pouch.repository';
import { Challenge } from 'app/domain/entities/challenge';

export const CHALLENGECRUD = createRepositoryToken(
    'ChallengeRepository',
    () => new PouchRepository<Challenge, number>('challenges'),
);
