import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { Observable, from, of } from 'rxjs';
import { switchMap, map, catchError, tap } from 'rxjs/operators';
import PouchDB from 'pouchdb';
import { PersistenceWrapper } from './PersistenceWrapper';

export class PouchPersistentStorage implements PersistentStorage {
    db: PouchDB.Database<PersistenceWrapper<unknown>> = new PouchDB<PersistenceWrapper<unknown>>('persistence', {
        auto_compaction: true,
    });

    put<T>(key: string, val: T): Observable<boolean> {
        return from(this.db.get(key)).pipe(
            catchError(_ => of(new PersistenceWrapper(key, val, 'persistence'))),
            switchMap(w => from(this.db.put(new PersistenceWrapper(w._id, val, w.type, w._rev)))),
            catchError(() => {
                return of({ id: null, rev: null, ok: false });
            }),
            map(res => res.ok),
        );
    }

    get<T>(key: string): Observable<T | null> {
        return from(this.db.get(key)).pipe(
            catchError(() => of(null)),
            map(w => w?.value as T | null),
        );
    }
}
