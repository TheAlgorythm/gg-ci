import { Identity } from 'app/framework/interfaces/identity';
import { Stringifyable } from 'app/framework/interfaces/stringifyable';

export class DbWrapper<TIdentity extends Stringifyable, T extends Identity<T, TIdentity>> {
    public _id: string;
    public value: T;
    public _rev?: string;
    constructor(val: T, rev?: string) {
        this._id = val.id.toString();
        this.value = val;
        this._rev = rev;
    }
}
