import { Info } from 'app/domain/entities/info';
import { PouchRepository } from './pouch.repository';
import { createRepositoryToken } from 'app/framework/interfaces/repository';

export const INFOCRUD = createRepositoryToken('InfoRepository', () => new PouchRepository<Info, number>('infos'));
