import { createRepositoryToken } from 'app/framework/interfaces/repository';
import { PouchRepository } from './pouch.repository';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export const WEEKLYCHALLENGECRUD = createRepositoryToken(
    'WeeklyChallengeRepository',
    () => new PouchRepository<WeeklyChallenge, number>('weeklyChallenges'),
);
