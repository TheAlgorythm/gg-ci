export class PersistenceWrapper<T> {
    value: T;
    type: string;
    _id: string;
    _rev?: string;
    constructor(id: string, val: T, type: string, rev?: string) {
        this.value = val;
        this.type = type;
        this._id = id;
        this._rev = rev;
    }
}
