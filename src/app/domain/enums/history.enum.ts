export enum HistoryState {
    'Success',
    'Failure',
}

export enum ChallengeType {
    'Daily',
    'Weekly',
}
