export class Category {
    public static categories: Category[] = [];

    static readonly CONSUME = new Category(0, 'consume', '/assets/images/Icon02a_Consume.svg');
    static readonly DIET = new Category(1, 'diet', '/assets/images/Icon02a_Diet.svg');
    static readonly MOBILITY = new Category(2, 'mobility', '/assets/images/Icon02a_Mobility.svg');
    static readonly HOUSHOLD = new Category(3, 'household', '/assets/images/Icon02a_Household.svg');
    static readonly KNOWLEDGE = new Category(4, 'knowledge', '/assets/images/Icon02a_Knowledge.svg');

    private constructor(readonly id: number, readonly name: string, readonly imagePath: string) {
        Category.categories[id] = this;
    }
}
