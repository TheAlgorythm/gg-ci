import { SequentialIdentity } from 'app/framework/interfaces/identity';

export type Hint = string;

export class Challenge implements SequentialIdentity<Challenge> {
    id: number;
    title: string;
    shortDescription: string;
    categoryId: number;
    infoId: number;
    hints: Hint[];

    constructor(data: Partial<Challenge>) {
        this.id = data.id ?? 0;
        this.title = data.title ?? '';
        this.shortDescription = data.shortDescription ?? '';
        this.categoryId = data.categoryId ?? 0;
        this.infoId = data.infoId ?? 0;
        this.hints = data.hints ?? [];
    }
}

export type Choice = [Challenge, Challenge];

export type SelectedChallengeOrChoice = Challenge | Choice;
