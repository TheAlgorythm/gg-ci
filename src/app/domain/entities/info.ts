import { SequentialIdentity } from 'app/framework/interfaces/identity';

export class Info implements SequentialIdentity<Info> {
    id: number;
    title: string;
    text: string;
    shortDescription: string;

    constructor(data: Required<Info>) {
        this.id = data.id;
        this.title = data.title;
        this.text = data.text;
        this.shortDescription = data.shortDescription;
    }
}
