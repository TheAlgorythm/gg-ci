export enum GlobalState {
    'initializing',
    'initialized',
}

export class GlobalModelState {
    public state: GlobalState;
    constructor(data: { state?: GlobalState } = {}) {
        this.state = data.state ?? GlobalState.initializing;
    }
}

export const initialState: GlobalModelState = new GlobalModelState();
