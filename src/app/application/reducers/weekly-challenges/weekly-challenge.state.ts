import { WeeklyChallenge } from './../../../domain/entities/weekly-challenge';

export enum WeeklyChallengeState {
    'teaser',
    'active',
    'complete',
}

export class WeeklyChallengeModelState {
    constructor(
        public state: WeeklyChallengeState | null = null,
        public current: WeeklyChallenge | null = null,
        public startDate: Date | null = null,
        public lastChange: Date = new Date(),
    ) {}
}

export function getWeeklyChallengeModelState(): WeeklyChallengeModelState {
    return new WeeklyChallengeModelState();
}
