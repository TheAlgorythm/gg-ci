import { createReducer, on, Action } from '@ngrx/store';
import {
    getWeeklyChallengeModelState,
    WeeklyChallengeModelState,
    WeeklyChallengeState,
} from './weekly-challenge.state';
import {
    afterLoadedTeaser,
    afterLoadedWeeklyChallengeState,
    loadActiveChallenge,
    afterCompleteChallenge,
} from '@actions/weekly-challenges';

const weeklyChallengeReducer = createReducer(
    getWeeklyChallengeModelState(),
    on(
        afterLoadedTeaser,
        (state: WeeklyChallengeModelState, { weeklyChallenge, startDate }) =>
            new WeeklyChallengeModelState(WeeklyChallengeState.teaser, weeklyChallenge, startDate),
    ),
    on(
        afterLoadedWeeklyChallengeState,
        (state: WeeklyChallengeModelState, { weeklyChallengeState }) =>
            new WeeklyChallengeModelState(
                weeklyChallengeState.state,
                weeklyChallengeState.current,
                weeklyChallengeState.startDate,
            ),
    ),
    on(
        loadActiveChallenge,
        (state: WeeklyChallengeModelState, { weeklyChallenge, startDate }) =>
            new WeeklyChallengeModelState(WeeklyChallengeState.active, weeklyChallenge, startDate),
    ),
    on(
        afterCompleteChallenge,
        (state: WeeklyChallengeModelState, { weeklyChallenge, startDate }) =>
            new WeeklyChallengeModelState(WeeklyChallengeState.complete, weeklyChallenge, startDate),
    ),
);

export function reducer(state: WeeklyChallengeModelState | undefined, action: Action): WeeklyChallengeModelState {
    return weeklyChallengeReducer(state, action);
}
