import { DataMigration } from './seeder';
import { Challenge } from 'app/domain/entities/challenge';
import { Inject, Injectable } from '@angular/core';
import { Repository } from 'app/framework/interfaces/repository';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { BaseSeeder } from './base.seeder';
import dailyJson from 'app/application/data/daily-challenges.json';
import { CHALLENGECRUD } from 'app/infrastructure/challenge-crud';

@Injectable({
    providedIn: 'root',
})
export class ChallengeSeeder extends BaseSeeder<Challenge> {
    constructor(@Inject(CHALLENGECRUD) repo: Repository<Challenge, number>, persistence: PersistentStorage) {
        super(dailyJson as DataMigration<Challenge>, persistence, repo);
    }
}
