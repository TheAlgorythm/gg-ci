import { Injectable } from '@angular/core';
import { SequentialIdentity } from 'app/framework/interfaces/identity';

export interface DataMigration<T extends SequentialIdentity<T>> {
    version: number;
    versionKey: string;
    data: T[];
}

export enum SeedState {
    'success',
    'noSeeding',
    'withErrors',
}
export interface SeedResult {
    seedingResult: SeedState;
    errors: Error[];
    canBeSaved(): boolean;
}

@Injectable()
export abstract class Seeder {
    public abstract seed(): Promise<SeedResult>;
}
