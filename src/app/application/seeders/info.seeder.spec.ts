import { INFOCRUD } from '../../infrastructure/info-repository';
import { InfoSeeder } from './info.seeder';
import { SpectatorService } from '@ngneat/spectator';
import { createServiceFactory } from '@ngneat/spectator/jest';
import { of } from 'rxjs/internal/observable/of';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { SeedState } from '../seeders/seeder';
import { throwError } from 'rxjs';
import { repositoryMock } from 'app/infrastructure/__mocks__/repository.mock';
import { persistenceMock } from 'app/infrastructure/__mocks__/persistence.mock';
jest.mock('../data/infos.json', () => {
    return {
        version: 2,
        versionKey: 'myKey',
        data: [
            {
                id: 0,
                title: 'Test title',
                shortDescription: 'Test description',
                categoryId: 1,
                infoId: 1,
            },
        ],
    };
});

describe('InfoSeeder', () => {
    let spectator: SpectatorService<InfoSeeder>;

    const infoMock = {
        version: 2,
        versionKey: 'myKey',
        data: [
            {
                id: 0,
                title: 'Test title',
                shortDescription: 'Test description',
                categoryId: 1,
                infoId: 1,
            },
        ],
    };

    const createService = createServiceFactory({
        service: InfoSeeder,
        providers: [
            { provide: INFOCRUD, useValue: repositoryMock },
            { provide: PersistentStorage, useValue: persistenceMock },
        ],
    });

    beforeEach(() => {
        spectator = createService();
    });

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should add all infos to the database', async () => {
        repositoryMock.addOrUpdate.mockReturnValue(of(true));
        repositoryMock.all.mockReturnValue(of([]));
        persistenceMock.get.mockReturnValue(of(1));
        persistenceMock.put.mockReturnValue(of(true));

        const result = await spectator.service.seed();

        expect(result.seedingResult).toEqual(SeedState.success);

        expect(persistenceMock.get).toHaveBeenCalledWith('myKey');
        expect(persistenceMock.put).toHaveBeenCalledWith('myKey', 2);

        expect(repositoryMock.addOrUpdate).toHaveBeenCalledWith(infoMock.data[0]);
        expect(repositoryMock.all).toHaveBeenCalled();
    });

    it('should return false if an error occurs', async () => {
        repositoryMock.addOrUpdate.mockReturnValue(throwError('error'));

        console.log = jest.fn();
        const result = await spectator.service.seed();
        expect(result.seedingResult).toEqual(SeedState.withErrors);
    });
});
