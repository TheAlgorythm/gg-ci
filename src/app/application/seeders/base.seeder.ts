import { Observable, forkJoin, of, OperatorFunction } from 'rxjs';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { SequentialIdentity } from 'app/framework/interfaces/identity';
import { Seeder, DataMigration, SeedResult, SeedState } from '../seeders/seeder';
import { Repository } from 'app/framework/interfaces/repository';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import { SingleResult } from '../services/single-result';
import { DefaultSeedResult } from './default.seedresult';

export abstract class BaseSeeder<T extends SequentialIdentity<T>> extends Seeder {
    constructor(
        private migration: DataMigration<T>,
        private persistence: PersistentStorage,
        private repo: Repository<T, number>,
    ) {
        super();
    }

    public async seed(): Promise<SeedResult> {
        const versionToApply = this.migration.version;
        const currentVersion = (await this.persistence.get<number>(this.migration.versionKey).toPromise()) ?? 0;
        if (currentVersion < versionToApply) {
            // TODO: what to do if seeding fails
            const result = await this.seedData(this.migration.data).toPromise();
            if (result.canBeSaved()) {
                await this.persistence.put(this.migration.versionKey, versionToApply).toPromise();
            }
            return result;
        }
        return { seedingResult: SeedState.noSeeding, errors: [], canBeSaved: () => false };
    }

    protected seedData(data: T[]): Observable<SeedResult> {
        return forkJoin([this.addOrUpdateAll(data), this.deleteIdsNotInList(data)]).pipe(
            tap(([a, b]) => console.table([...a, ...b])),
            map(
                ([addresults, deleteResults]) =>
                    new DefaultSeedResult(this.filterAndMapErros([...addresults, ...deleteResults])),
            ),
        );
    }

    private filterAndMapErros(results: SingleResult[]): Error[] {
        return results.filter(x => !x.success && !!x.error).map(x => x.error as Error);
    }

    private deleteIdsNotInList(data: T[]): Observable<SingleResult[]> {
        return this.repo.all().pipe(
            map(databaseValues => databaseValues.filter(dbVal => data.every(dataEntry => dataEntry.id !== dbVal.id))),
            switchMap(toDelete => {
                if (toDelete.length === 0) {
                    return of([]);
                }
                return forkJoin(toDelete.map(d => this.repo.delete(d).pipe(this.mapToSingleResult())));
            }),
        );
    }

    private addOrUpdateAll(data: T[]): Observable<SingleResult[]> {
        return forkJoin(
            data.map(dataEntry => {
                return this.repo.addOrUpdate(dataEntry).pipe(this.mapToSingleResult());
            }),
        );
    }

    private mapToSingleResult(): OperatorFunction<boolean, SingleResult> {
        return source =>
            source.pipe(
                map(_ => new SingleResult()),
                catchError(err => of(new SingleResult(err))),
            );
    }
}
