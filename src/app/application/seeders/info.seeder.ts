import { DataMigration } from '../seeders/seeder';
import { INFOCRUD } from '../../infrastructure/info-repository';
import { Injectable, Inject } from '@angular/core';
import { Repository } from 'app/framework/interfaces/repository';
import { Info } from 'app/domain/entities/info';
import infoJson from '../data/infos.json';
import { BaseSeeder } from './base.seeder';
import { PersistentStorage } from 'app/framework/interfaces/persistence';

@Injectable({
    providedIn: 'root',
})
export class InfoSeeder extends BaseSeeder<Info> {
    constructor(@Inject(INFOCRUD) infoRepository: Repository<Info, number>, persistence: PersistentStorage) {
        super(infoJson as DataMigration<Info>, persistence, infoRepository);
    }
}
