import { SeedResult, SeedState } from './seeder';

export class DefaultSeedResult implements SeedResult {
    seedingResult: SeedState;
    errors: Error[];

    constructor(errors?: Error[]) {
        this.seedingResult = !!errors && errors.length === 0 ? SeedState.success : SeedState.withErrors;
        this.errors = errors ?? [];
    }
    canBeSaved(): boolean {
        return this.seedingResult === SeedState.success;
    }
}
