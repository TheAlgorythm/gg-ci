import { ChallengeService } from '../services/challenge.service';
import { Observable, of } from 'rxjs';
import { Challenge, Choice } from '../../domain/entities/challenge';
import { getRandomChallenge } from './challenge.mock';
import { SequentialIdentity } from 'app/framework/interfaces/identity';
import { delay, map } from 'rxjs/operators';
import { addSeconds } from 'date-fns';
export class ChallengeRepoMock implements ChallengeService {
    public complete(id: SequentialIdentity<Challenge>, didSucceed: boolean): Observable<Date> {
        return of(this.getNextChoiceDate()).pipe(delay(this.getDelay()));
    }

    public getChoice(): Observable<Choice> {
        const [firstChallenge, firstChallengeIndex] = getRandomChallenge(null);
        return of([firstChallenge, getRandomChallenge(firstChallengeIndex)[0]]).pipe(
            delay(this.getDelay()),
            map(x => [x[0], x[1]]),
        );
    }

    // for real data we probably want to calculate delta
    private getNextChoiceDate(): Date {
        return addSeconds(new Date(), 10);
    }

    private getDelay(): number {
        return 10;
    }
}
