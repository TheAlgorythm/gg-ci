import { Action } from '@ngrx/store';

export class MockDispatcher {
    public dispatch(action: Action): void {}
}
