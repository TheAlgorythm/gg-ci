import { Injectable, Inject } from '@angular/core';
import { WEEKLYCHALLENGECRUD } from 'app/infrastructure/weekly-challenge.repository';
import { Repository } from 'app/framework/interfaces/repository';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class WeeklyChallengeService {
    constructor(@Inject(WEEKLYCHALLENGECRUD) private repo: Repository<WeeklyChallenge, number>) {}

    getWeeklyChallenge(): Observable<WeeklyChallenge> {
        return this.repo
            .all()
            .pipe(map(weeklyChallenges => weeklyChallenges[this.getRandomIndex(weeklyChallenges.length)]));
    }

    private getRandomIndex(length: number): number {
        return Math.floor(Math.random() * length);
    }
}
