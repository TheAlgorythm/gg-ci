export class SingleResult {
    success: boolean;
    error?: Error;

    constructor(error?: Error) {
        this.success = !error;
        this.error = error;
    }
}
