import { createAction, props } from '@ngrx/store';
import { DailyChallengeModelState } from 'app/application/reducers/daily-challenges/daily-challenge.state';

export const afterLoadState = createAction(
    '[Daily-Challenge] AfterLoadState',
    props<{
        dailyChallengeState: DailyChallengeModelState;
        startDate: Date | null;
    }>(),
);

export default afterLoadState;
