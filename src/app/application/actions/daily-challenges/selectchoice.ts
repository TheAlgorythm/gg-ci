import { createAction, props } from '@ngrx/store';
import { Challenge } from 'app/domain/entities/challenge';

export const selectChoice = createAction(
    '[Challenge Daily] SelectChoice',
    props<{ challenge: Challenge; startDate: Date }>(),
);

export default selectChoice;
