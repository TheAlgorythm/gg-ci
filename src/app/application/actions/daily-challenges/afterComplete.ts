import { createAction, props } from '@ngrx/store';
import { Challenge } from 'app/domain/entities/challenge';

export const afterComplete = createAction(
    '[Challenge Daily] AfterComplete',
    props<{ challenge: Challenge; startDate: Date }>(),
);

export default afterComplete;
