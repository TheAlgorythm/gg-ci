import { createAction } from '@ngrx/store';

export const saveState = createAction('[Global] SaveState');

export default saveState;
