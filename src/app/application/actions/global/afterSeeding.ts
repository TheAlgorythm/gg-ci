import { createAction } from '@ngrx/store';

export const afterSeeding = createAction('[Global] afterSeeding');

export default afterSeeding;
