import { createAction } from '@ngrx/store';

export const firstTime = createAction('[Global] FirstTime');

export default firstTime;
