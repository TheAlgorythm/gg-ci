import { createAction } from '@ngrx/store';

export const loadTeaser = createAction('[Weekly-Challenge] LoadTeaser');
export default loadTeaser;
