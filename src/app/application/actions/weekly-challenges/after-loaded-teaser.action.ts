import { createAction, props } from '@ngrx/store';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export const afterLoadedTeaser = createAction(
    '[Weekly-Challenge] AfterLoadedTeaser',
    props<{ weeklyChallenge: WeeklyChallenge; startDate: Date }>(),
);
export default afterLoadedTeaser;
