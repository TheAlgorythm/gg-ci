import { createAction, props } from '@ngrx/store';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export const afterCompleteChallenge = createAction(
    '[Weekly-Challenge] AfterComplete',
    props<{ weeklyChallenge: WeeklyChallenge; startDate: Date }>(),
);

export default afterCompleteChallenge;
