import { createAction, props } from '@ngrx/store';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

export const loadActiveChallenge = createAction(
    '[Weekly-Challenge] LoadActiveChallenge',
    props<{ weeklyChallenge: WeeklyChallenge; startDate: Date }>(),
);
export default loadActiveChallenge;
