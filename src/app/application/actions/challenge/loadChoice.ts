import { createAction } from '@ngrx/store';

export const loadChoice = createAction('[Challenge] LoadChoice');
export default loadChoice;
