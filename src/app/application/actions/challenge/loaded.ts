import { createAction, props } from '@ngrx/store';
import { Challenge } from 'app/domain/entities/challenge';

export const loadedChallenge = createAction('[Challenge] Loaded', props<{ challenge: Challenge }>());
export default loadedChallenge;
