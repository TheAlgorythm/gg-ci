import { WeeklyChallengeModelState } from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { createFeatureSelector } from '@ngrx/store';

export const weeklyChallengeFeature = 'weekly-challenge';

export const getWeeklyChallengeState = createFeatureSelector<WeeklyChallengeModelState>(weeklyChallengeFeature);
