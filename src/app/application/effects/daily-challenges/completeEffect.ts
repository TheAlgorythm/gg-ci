import { Injectable } from '@angular/core';
import { ofType, Actions, Effect } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { complete, afterComplete } from '@actions/daily-challenges';
import { of } from 'rxjs';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { ChallengeType, HistoryState } from 'app/domain/enums/history.enum';

@Injectable()
export class CompleteEffect {
    @Effect()
    public loadEffect$ = this.actions$.pipe(
        ofType(complete),
        switchMap(action => {
            this.repo.complete(action.challenge, ChallengeType.Daily, HistoryState.Success);
            return of(afterComplete({ challenge: action.challenge, startDate: action.startDate }));
        }),
    );

    constructor(private actions$: Actions, private repo: ChallengeHistoryRepository) {}
}
