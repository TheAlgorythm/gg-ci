import { Injectable } from '@angular/core';
import { createEffect, ofType, Actions } from '@ngrx/effects';
import { loadChoice, loadedChoice } from '@actions/challenge';
import { switchMap } from 'rxjs/operators';
import { ChallengeService } from 'app/application/services/challenge.service';
import { merge, of } from 'rxjs';

@Injectable()
export class ChoiceLoadEffect {
    public loadEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadChoice),
            switchMap(() => this.repo.getChoice()),
            switchMap(choice => merge(of(loadedChoice({ choice, startDate: new Date() })))),
        ),
    );

    constructor(private actions$: Actions, private repo: ChallengeService) {}
}
