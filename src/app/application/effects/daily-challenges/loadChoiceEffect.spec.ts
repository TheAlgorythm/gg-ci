import { Choice } from 'app/domain/entities/challenge';
import { ChoiceLoadEffect } from 'app/application/effects/daily-challenges/loadChoiceEffect';
import { provideMockActions } from '@ngrx/effects/testing';
import { TestBed } from '@angular/core/testing';
import { Challenge } from './../../../domain/entities/challenge';
import { cold, hot } from 'jest-marbles';
import { loadedChoice, loadChoice } from '@actions/challenge';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { ChallengeService } from 'app/application/services/challenge.service';

describe('LoadChoiceEffect', () => {
    const pouchRepositoryMock = {
        getChoice: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: ChoiceLoadEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ChoiceLoadEffect,
                provideMockActions(() => actions$),
                { provide: ChallengeService, useValue: pouchRepositoryMock },
            ],
        });
        effect = TestBed.inject(ChoiceLoadEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from loadChoice to loadedChoice', () => {
        const startDate = new Date();
        const challenge1 = new Challenge({
            id: 1,
        });
        const challenge2 = new Challenge({
            id: 2,
        });
        const choice = [challenge1, challenge2] as Choice;
        const loadChoiceAction = loadChoice();
        const loadedChoiceAction = loadedChoice({ choice, startDate });

        jest.spyOn(global, 'Date').mockImplementation(() => (startDate as unknown) as string);

        actions$ = hot('-a', { a: loadChoiceAction });
        pouchRepositoryMock.getChoice.mockReturnValue(of(choice));
        const expected$ = cold('-b', { b: loadedChoiceAction });

        expect(effect.loadEffect$).toBeObservable(expected$);
    });
});
