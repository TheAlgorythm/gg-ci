import { Challenge } from 'app/domain/entities/challenge';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { CompleteEffect } from './completeEffect';
import { complete, afterComplete } from '@actions/daily-challenges';
import { Action } from '@ngrx/store';
import { hot, cold } from 'jest-marbles';
import { ChallengeService } from 'app/application/services/challenge.service';

describe('CompleteEffect', () => {
    const pouchRepositoryMock = {
        complete: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: CompleteEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                CompleteEffect,
                provideMockActions(() => actions$),
                { provide: ChallengeService, useValue: pouchRepositoryMock },
            ],
        });
        effect = TestBed.inject(CompleteEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from complete to afterComplete', () => {
        const startDate = new Date();
        const challenge = new Challenge({
            id: 1,
        });
        const completeAction = complete({ challenge, startDate });
        const afterCompleteAction = afterComplete({ challenge, startDate });

        actions$ = hot('-a', { a: completeAction });
        const expected$ = cold('-b', { b: afterCompleteAction });

        expect(effect.loadEffect$).toBeObservable(expected$);
    });
});
