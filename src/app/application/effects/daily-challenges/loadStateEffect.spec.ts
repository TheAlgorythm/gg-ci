import { dailyChallengeFeature } from 'app/application/selectors/daily-challenges/selector';
import { loadChallengeState } from './../../actions/daily-challenges/loadState';
import { hot, cold } from 'jest-marbles';
import { loadChoice } from './../../actions/challenge/loadChoice';
import { afterLoadState } from './../../actions/daily-challenges/afterLoadState';
import { DailyChallengeModelState, DailyChallengeState } from './../../reducers/daily-challenges/daily-challenge.state';
import { provideMockActions } from '@ngrx/effects/testing';
import { LoadStateEffect } from './loadStateEffect';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { Challenge } from 'app/domain/entities/challenge';
import { GlobalState } from 'app/application/reducers/global/global.state';

describe('LoadStateEffect', () => {
    const persistanceStorageMock = {
        get: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: LoadStateEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                LoadStateEffect,
                provideMockActions(() => actions$),
                { provide: PersistentStorage, useValue: persistanceStorageMock },
            ],
        });
        effect = TestBed.inject(LoadStateEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from loadChallengeState to loadChoice if state is null', () => {
        const loadChallengeStateAction = loadChallengeState();
        const loadChoiceAction = loadChoice();

        actions$ = hot('-a', { a: loadChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(of(null));
        const expected$ = cold('-(b)', {
            b: loadChoiceAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });

    it('should change action from loadChallengeState to afterLoadState if state is null', () => {
        const startDate = new Date();
        const challenge = new Challenge({});
        const dailyChallengeState = new DailyChallengeModelState({
            current: challenge,
            state: DailyChallengeState.selected,
            startDate,
        });
        const loadChallengeStateAction = loadChallengeState();
        const afterLoadStateAction = afterLoadState({ dailyChallengeState, startDate });

        actions$ = hot('-a', { a: loadChallengeStateAction });
        persistanceStorageMock.get.mockReturnValue(
            of({
                [dailyChallengeFeature]: dailyChallengeState,
            }),
        );

        const expected$ = cold('-(b)', {
            b: afterLoadStateAction,
        });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });
});
