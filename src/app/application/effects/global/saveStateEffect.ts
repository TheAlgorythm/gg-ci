import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { saveState, afterSaveState } from '@actions/global';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import { loadedChoice } from '@actions/challenge';
import { afterComplete, selectChoice } from '@actions/daily-challenges';
import { afterLoadedTeaser, loadActiveChallenge, afterCompleteChallenge } from '@actions/weekly-challenges';

@Injectable()
export class SaveStateEffect {
    public stateEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(
                saveState,
                loadedChoice,
                afterComplete,
                selectChoice,
                afterLoadedTeaser,
                loadActiveChallenge,
                afterCompleteChallenge,
            ),
            withLatestFrom(this.store),
            switchMap(([_, state]) => {
                return this.persistence.put('state', state).pipe(
                    switchMap(() => {
                        return of(afterSaveState());
                    }),
                );
            }),
        ),
    );

    constructor(private actions$: Actions, private store: Store<{}>, private persistence: PersistentStorage) {}
}
