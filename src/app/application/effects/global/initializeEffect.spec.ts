import { afterInitialize } from './../../actions/global/afterInitialize';
import { hot, cold } from 'jest-marbles';
import { firstTime } from './../../actions/global/firsttime';
import { initialize } from './../../actions/global/initialize';
import { provideMockActions } from '@ngrx/effects/testing';
import { InitializerEffect } from './initializeEffect';
import { Observable, of } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { seed } from '@actions/global';

describe('InitializerEffect', () => {
    const persistanceStorageMock = {
        get: jest.fn(),
        put: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: InitializerEffect;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InitializerEffect,
                provideMockActions(() => actions$),
                { provide: PersistentStorage, useValue: persistanceStorageMock },
            ],
        });
        effect = TestBed.inject(InitializerEffect);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from initialize to firstTime if state is false', () => {
        const initializeAction = initialize();
        const outcome = firstTime();

        actions$ = hot('-a', { a: initializeAction });
        persistanceStorageMock.get.mockReturnValue(of(false));
        const expected$ = cold('-b', { b: outcome });

        expect(effect.loadEffect$).toBeObservable(expected$);
    });

    it('should change action from initialize to afterInitialize if state is true', () => {
        const initializeAction = initialize();

        actions$ = hot('-a', { a: initializeAction });
        persistanceStorageMock.get.mockReturnValue(of(true));
        const expected$ = cold('-(bc)', { b: seed(), c: afterInitialize() });

        expect(effect.loadEffect$).toBeObservable(expected$);
    });
});
