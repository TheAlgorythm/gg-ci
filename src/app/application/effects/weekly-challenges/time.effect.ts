import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {
    afterLoadedWeeklyChallengeState,
    loadActiveChallenge,
    timeIsNotOver,
    loadTeaser,
    afterCompleteChallenge,
} from '@actions/weekly-challenges';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { PeriodOfTimeCheckerService } from 'app/services/period-of-time-checker.service';
import { environment } from 'environments/environment';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { ChallengeType, HistoryState } from 'app/domain/enums/history.enum';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { getWeeklyChallengeState } from '@selectors/weekly-challenges/selector';
import {
    WeeklyChallengeModelState,
    WeeklyChallengeState,
} from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

@Injectable()
export class TimeEffect {
    public timeEffect$ = createEffect(() =>
        this.actions$.pipe(
            ofType(afterLoadedWeeklyChallengeState, loadActiveChallenge, afterCompleteChallenge),
            switchMap(() => {
                return this.persistence.get<object>('state').pipe(
                    switchMap((state: object | null) => {
                        if (state === null) {
                            return of(null);
                        }
                        const weeklyChallengeModelState = getWeeklyChallengeState(state);
                        return of(weeklyChallengeModelState);
                    }),
                );
            }),
            switchMap((weeklyChallengeModelState: WeeklyChallengeModelState | null) => {
                if (null === weeklyChallengeModelState) {
                    return of(loadTeaser());
                }
                const startDate = weeklyChallengeModelState.startDate;

                if (null === startDate) {
                    return of(loadTeaser());
                }

                if (this.isTimeOver(startDate)) {
                    this.completeChallenge(weeklyChallengeModelState.state, weeklyChallengeModelState.current);
                    return of(loadTeaser());
                }

                return of(timeIsNotOver());
            }),
        ),
    );

    constructor(
        private actions$: Actions,
        private periodOfTimeChecker: PeriodOfTimeCheckerService,
        private historyRepository: ChallengeHistoryRepository,
        private persistence: PersistentStorage,
    ) {}

    private isTimeOver(startDate: Date): boolean {
        return this.periodOfTimeChecker.isTimeOver(
            new Date(),
            startDate,
            environment.timeTypeForWeeklyChallenge,
            environment.maxTimeForWeeklyChallenge,
        );
    }

    private completeChallenge(state: WeeklyChallengeState | null, weeklyChallenge: WeeklyChallenge | null): void {
        // Ignore challenge if it was completed
        if (state === WeeklyChallengeState.complete || null === weeklyChallenge) {
            return;
        }

        this.historyRepository.complete(weeklyChallenge, ChallengeType.Weekly, HistoryState.Failure);
    }
}
