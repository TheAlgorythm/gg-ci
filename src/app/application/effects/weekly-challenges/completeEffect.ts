import { Injectable } from '@angular/core';
import { ofType, Actions, Effect } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { ChallengeType, HistoryState } from 'app/domain/enums/history.enum';
import { completeChallenge, afterCompleteChallenge } from '@actions/weekly-challenges';

@Injectable()
export class CompleteEffect {
    @Effect()
    public loadEffect$ = this.actions$.pipe(
        ofType(completeChallenge),
        switchMap(action => {
            const weeklyChallenge = action.weeklyChallenge;
            const startDate = action.startDate;

            this.repo.complete(weeklyChallenge, ChallengeType.Weekly, HistoryState.Success);

            return of(afterCompleteChallenge({ weeklyChallenge, startDate }));
        }),
    );

    constructor(private actions$: Actions, private repo: ChallengeHistoryRepository) {}
}
