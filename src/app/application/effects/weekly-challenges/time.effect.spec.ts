import { TimeEffect } from './time.effect';
import { PeriodOfTimeCheckerService } from 'app/services/period-of-time-checker.service';
import { TestBed } from '@angular/core/testing';
import { Action } from '@ngrx/store';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { afterLoadedWeeklyChallengeState, loadTeaser, timeIsNotOver } from '@actions/weekly-challenges';
import {
    WeeklyChallengeModelState,
    WeeklyChallengeState,
} from 'app/application/reducers/weekly-challenges/weekly-challenge.state';
import { hot, cold } from 'jest-marbles';
import { PersistentStorage } from 'app/framework/interfaces/persistence';
import { ChallengeHistoryRepository } from 'app/infrastructure/challenge-history.repository';
import { weeklyChallengeFeature } from '@selectors/weekly-challenges/selector';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';

describe('TimeEffect', () => {
    const persistanceStorageMock = {
        get: jest.fn(),
    };

    let actions$: Observable<Action>;
    let effect: TimeEffect;
    let periodOfTimeCheckerSpy: jest.SpyInstance<unknown>;
    const historyRepositoryMock = {
        complete: jest.fn(),
        completeChoice: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                TimeEffect,
                provideMockActions(() => actions$),
                PeriodOfTimeCheckerService,
                { provide: PersistentStorage, useValue: persistanceStorageMock },
                { provide: ChallengeHistoryRepository, useValue: historyRepositoryMock },
            ],
        });
        effect = TestBed.inject(TimeEffect);
        periodOfTimeCheckerSpy = jest.spyOn(TestBed.inject(PeriodOfTimeCheckerService), 'isTimeOver');
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from afterLoadedWeeklyChallengeState to loadTeaser if startDate is null', () => {
        const weeklyChallengeState = new WeeklyChallengeModelState();
        const loadedWeeklyChallengeStateAction = afterLoadedWeeklyChallengeState({
            weeklyChallengeState,
            startDate: null,
        });
        const loadTeaserAction = loadTeaser();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [weeklyChallengeFeature]: weeklyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: loadedWeeklyChallengeStateAction });
        const expected$ = cold('-b', { b: loadTeaserAction });

        expect(effect.timeEffect$).toBeObservable(expected$);
    });

    it('should change action from afterLoadedWeeklyChallengeState to loadTeaser if time is over', () => {
        const startDate = new Date();
        const weeklyChallenge = new WeeklyChallenge({});
        const weeklyChallengeState = new WeeklyChallengeModelState(
            WeeklyChallengeState.active,
            weeklyChallenge,
            startDate,
        );

        const loadedWeeklyChallengeStateAction = afterLoadedWeeklyChallengeState({ weeklyChallengeState, startDate });
        const loadTeaserAction = loadTeaser();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [weeklyChallengeFeature]: weeklyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: loadedWeeklyChallengeStateAction });
        const expected$ = cold('-b', { b: loadTeaserAction });

        periodOfTimeCheckerSpy.mockReturnValue(true);

        expect(effect.timeEffect$).toBeObservable(expected$);
    });

    it('should change action from afterLoadedWeeklyChallengeState to timeIsNotOver', () => {
        const startDate = new Date();
        const weeklyChallenge = new WeeklyChallenge({});
        const weeklyChallengeState = new WeeklyChallengeModelState(
            WeeklyChallengeState.active,
            weeklyChallenge,
            startDate,
        );

        const loadedWeeklyChallengeStateAction = afterLoadedWeeklyChallengeState({ weeklyChallengeState, startDate });
        const timeIsNotOverAction = timeIsNotOver();

        persistanceStorageMock.get.mockReturnValue(
            of({
                [weeklyChallengeFeature]: weeklyChallengeState,
            }),
        );

        actions$ = hot('-a', { a: loadedWeeklyChallengeStateAction });
        const expected$ = cold('-b', { b: timeIsNotOverAction });

        periodOfTimeCheckerSpy.mockReturnValue(false);

        expect(effect.timeEffect$).toBeObservable(expected$);
    });
});
