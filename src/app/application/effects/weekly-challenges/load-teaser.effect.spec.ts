import { LoadTeaserEffect } from './load-teaser.effect';
import { WeeklyChallengeService } from 'app/application/services/weekly-challenge.service';
import { Observable, of } from 'rxjs';
import { Action } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { loadTeaser, afterLoadedTeaser } from '@actions/weekly-challenges';
import { WeeklyChallenge } from 'app/domain/entities/weekly-challenge';
import { hot, cold } from 'jest-marbles';

describe('LoadTeaserEffect', () => {
    let actions$: Observable<Action>;
    let effect: LoadTeaserEffect;
    let weeklyChallengeServiceSpy: jest.SpyInstance<Observable<WeeklyChallenge>>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [LoadTeaserEffect, provideMockActions(() => actions$), WeeklyChallengeService],
        });
        effect = TestBed.inject(LoadTeaserEffect);
        weeklyChallengeServiceSpy = jest.spyOn(TestBed.inject(WeeklyChallengeService), 'getWeeklyChallenge');
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from loadTeaser to afterLoadedTeaser', () => {
        const weeklyChallenge = new WeeklyChallenge({
            id: 1,
        });
        const startDate = new Date(2013, 9, 23);
        const loadTeaserAction = loadTeaser();
        const loadedTeaserAction = afterLoadedTeaser({ weeklyChallenge, startDate });

        jest.spyOn(global, 'Date').mockImplementation(() => (startDate as unknown) as string);

        actions$ = hot('-a', { a: loadTeaserAction });
        weeklyChallengeServiceSpy.mockReturnValue(of(weeklyChallenge));
        const expected$ = cold('-b', { b: loadedTeaserAction });

        expect(effect.stateEffect$).toBeObservable(expected$);
    });
});
