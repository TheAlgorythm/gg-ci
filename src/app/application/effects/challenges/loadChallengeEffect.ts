import { Actions, ofType, Effect } from '@ngrx/effects';
import { Injectable, Inject } from '@angular/core';
import { loadChallenge, loadedChallenge } from '@actions/challenge';
import { switchMap, map } from 'rxjs/operators';
import { Repository } from 'app/framework/interfaces/repository';
import { Challenge } from 'app/domain/entities/challenge';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CHALLENGECRUD } from 'app/infrastructure/challenge-crud';

@Injectable()
export class ChallengeLoadEffects {
    @Effect()
    public loadEffect$: Observable<Action> = this.actions$.pipe(
        ofType(loadChallenge),
        switchMap(action => this.repo.get(action.id)),
        map(challenge => loadedChallenge({ challenge })),
    );

    constructor(private actions$: Actions, @Inject(CHALLENGECRUD) private repo: Repository<Challenge, number>) {}
}
