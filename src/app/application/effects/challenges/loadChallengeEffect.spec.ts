import { provideMockActions } from '@ngrx/effects/testing';
import { TestBed } from '@angular/core/testing';
import { loadedChallenge } from './../../actions/challenge/loaded';
import { Challenge } from './../../../domain/entities/challenge';
import { ChallengeLoadEffects } from './loadChallengeEffect';
import { cold, hot } from 'jest-marbles';
import { loadChallenge } from '@actions/challenge';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { CHALLENGECRUD } from 'app/infrastructure/challenge-crud';

describe('ChallengeLoadEffect', () => {
    const pouchRepositoryMock = {
        get: jest.fn(),
    };
    let actions$: Observable<Action>;
    let effect: ChallengeLoadEffects;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ChallengeLoadEffects,
                provideMockActions(() => actions$),
                { provide: CHALLENGECRUD, useValue: pouchRepositoryMock },
            ],
        });
        effect = TestBed.inject(ChallengeLoadEffects);
    });

    it('should create', () => {
        expect(effect).toBeTruthy();
    });

    it('should change action from loadChallenge to loadedChallenge', () => {
        const challenge = new Challenge({
            id: 1,
        });
        const loadChallengeAction = loadChallenge({ id: challenge });
        const outcome = loadedChallenge({ challenge });

        actions$ = hot('-a', { a: loadChallengeAction });
        const response$ = cold('-a|', { a: challenge });
        pouchRepositoryMock.get.mockReturnValue(response$);
        const expected$ = cold('--b', { b: outcome });

        expect(effect.loadEffect$).toBeObservable(expected$);
    });
});
