import { Observable, of } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { INFOCRUD } from 'app/infrastructure/info-repository';
import { Repository } from 'app/framework/interfaces/repository';
import { Info } from 'app/domain/entities/info';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class InfoResolverService implements Resolve<Info> {
    constructor(@Inject(INFOCRUD) private infoRepository: Repository<Info, number>) {}

    resolve(route: ActivatedRouteSnapshot): Observable<any> {
        const infoId = route.paramMap.get('id');

        if (null !== infoId) {
            return this.infoRepository.get({ id: parseInt(infoId, 10) });
        }
        return of(null);
    }
}
