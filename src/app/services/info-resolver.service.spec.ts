import { INFOCRUD } from './../infrastructure/info-repository';
import { Info } from './../domain/entities/info';
import { cold } from 'jest-marbles';
import { of } from 'rxjs';

import { InfoResolverService } from './info-resolver.service';
import { SpectatorService, createServiceFactory } from '@ngneat/spectator';
import { ActivatedRouteSnapshot } from '@angular/router';

describe('InfoResolverService', () => {
    const paramMapMock = {
        get: jest.fn(),
    };
    const routeMock = {
        get paramMap(): any {
            return paramMapMock;
        },
    };
    const pouchRepositoryMock = {
        get: jest.fn(),
    };

    let spectator: SpectatorService<InfoResolverService>;
    const createService = createServiceFactory({
        service: InfoResolverService,
        providers: [
            { provide: ActivatedRouteSnapshot, useValue: routeMock },
            { provide: INFOCRUD, useValue: pouchRepositoryMock },
        ],
    });

    beforeEach(() => (spectator = createService()));

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should return null if info not exist', () => {
        const route = spectator.inject(ActivatedRouteSnapshot);
        const expectedResult = cold('(a|)', { a: null });

        paramMapMock.get.mockReturnValue(null);

        expect(spectator.service.resolve(route)).toBeObservable(expectedResult);
    });

    it('should return info', () => {
        const route = spectator.inject(ActivatedRouteSnapshot);
        const info = new Info({ id: 1, title: 'i', text: 'am', shortDescription: 'batman' });

        paramMapMock.get.mockReturnValue(1);
        pouchRepositoryMock.get.mockReturnValue(of(info));

        const expectedResult = cold('(a|)', { a: info });
        expect(spectator.service.resolve(route)).toBeObservable(expectedResult);
    });
});
