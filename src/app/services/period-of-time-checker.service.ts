import { Injectable } from '@angular/core';
import { differenceInSeconds, differenceInMinutes, differenceInHours, differenceInCalendarDays } from 'date-fns';

@Injectable({
    providedIn: 'root',
})
export class PeriodOfTimeCheckerService {
    constructor() {}

    public isTimeOver(dateLeft: Date, dateRight: Date, timePeriodtype: string, maxTime: number): boolean {
        switch (timePeriodtype) {
            case 'seconds': {
                return maxTime <= differenceInSeconds(dateLeft, dateRight);
            }
            case 'minutes': {
                return maxTime <= differenceInMinutes(dateLeft, dateRight);
            }
            case 'hours': {
                return maxTime <= differenceInHours(dateLeft, dateRight);
            }
            case 'days': {
                return maxTime <= differenceInCalendarDays(dateLeft, dateRight);
            }
            default: {
                return false;
            }
        }
    }
}
