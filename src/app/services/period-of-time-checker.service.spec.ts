import { createServiceFactory, SpectatorService } from '@ngneat/spectator';

import { PeriodOfTimeCheckerService } from './period-of-time-checker.service';

describe('PeriodOfTimeCheckerService', () => {
    let spectator: SpectatorService<PeriodOfTimeCheckerService>;
    const createService = createServiceFactory(PeriodOfTimeCheckerService);

    beforeEach(() => (spectator = createService()));

    it('should be created', () => {
        expect(spectator.service).toBeTruthy();
    });

    it('should return false if ten seconds have not passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setSeconds(dateRight.getSeconds() - 5);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'seconds', 10)).toBeFalsy();
    });

    it('should return true if ten seconds have passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setSeconds(dateRight.getSeconds() - 10);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'seconds', 10)).toBeTruthy();
    });

    it('should return false if 10 minutes have not passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setMinutes(dateRight.getMinutes() - 5);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'minutes', 10)).toBeFalsy();
    });

    it('should return true if 10 minutes have passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setMinutes(dateRight.getMinutes() - 10);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'minutes', 10)).toBeTruthy();
    });

    it('should return false if 2 hour have not passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setHours(dateRight.getHours() - 1);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'hours', 2)).toBeFalsy();
    });

    it('should return true if 2 hour have passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setHours(dateRight.getHours() - 2);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'hours', 2)).toBeTruthy();
    });

    it('should return false if 1 day have not passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'days', 1)).toBeFalsy();
    });

    it('should return true if 1 day have passed', () => {
        const dateLeft = new Date();
        const dateRight = new Date();
        dateRight.setDate(dateRight.getDate() - 1);

        expect(spectator.service.isTimeOver(dateLeft, dateRight, 'days', 1)).toBeTruthy();
    });
});
