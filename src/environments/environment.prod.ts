export const environment = {
    production: true,
    timeTypeForChallenge: 'days',
    maxTimeForChallenge: 1,
    timeTypeForWeeklyChallenge: 'days',
    maxTimeForWeeklyChallenge: 7,
};
