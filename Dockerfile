FROM registry.gitlab.com/make-it-green/go-green/ionic:latest


WORKDIR /app

COPY . .

RUN npm install


WORKDIR /app

EXPOSE 8100 35729 53703 49153

CMD ["ionic","serve","--devapp"]